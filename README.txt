 Image Pyramid
 
 Description:
   This program computes the image pyramid of an input image using the 
   integral image (II). More precisely, at each algorithm iteration the 
   resized image is computed from the integral image (II) according to 
   the given cell size.

   If you make use of this code for research articles, we kindly encourage
   to cite the reference [1], listed below. This code is only for research
   and educational purposes.

 Steps:
   Steps to execute the program:
     1. Run the prg_setup.m file to configure the program paths and 
        compile the mex files.
     2. Run the prg_image_pyramid.m file to build the image pyramid 
        of a given input image. 

 References:
    [1] Computation of Rotation Local Invariant Features Using the Integral Image 
        for Real Time Object Detection
        M. Villamizar, A. Sanfeliu and J. Andrade-Cetto
        International Conference on Pattern Recognition (ICPR), 2006.

 Contact:
   Michael Villamizar
   mvillami-at-iri.upc.edu
   Institut de Robòtica i Informática Industrial CSIC-UPC
   Barcelona - Spain
   2014

