%% Image Pyramid
% 
% Description:
%   This program computes the image pyramid of an input image using the 
%   integral image (II). More precisely, at each algorithm iteration the 
%   resized image is computed from the integral image (II) according to 
%   the given cell size.
%
%   If you make use of this code for research articles, we kindly encourage
%   to cite the reference [1], listed below. This code is only for research
%   and educational purposes.
%
% Steps:
%   Steps to execute the program:
%     1. Run the prg_setup.m file to configure the program paths and 
%        compile the mex files.
%     2. Run the prg_image_pyramid.m file to build the image pyramid 
%        of a given input image. 
%
% References:
%    [1] Computation of Rotation Local Invariant Features Using the Integral Image 
%        for Real Time Object Detection
%        M. Villamizar, A. Sanfeliu and J. Andrade-Cetto
%        International Conference on Pattern Recognition (ICPR), 2006.
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2014
%

%% main function
function prg_image_pyramid()
clc,close all,clear all

% message
fun_messages('Image Pyramid','presentation');
fun_messages('Image Pyramid','title');

% parameters
fs = 10;  % font size
minCell = 1;  % min. cell size 
maxCell = 7;  % max. cell size 
imgHeight = 241;  % standard image height
minImgSize = 20;  % min. image size

% input image
img = imread('image.jpg');
img = fun_normalize(img);

% show input image
%figure,imshow(img);

% resize factor
resFactor = size(img,1)/imgHeight;

% resize image (standard height)
%img = imresize(img,inv(resFactor));
img = imresize(img,inv(resFactor),'bilinear');

% integral image computation
II = mex_img2II(img);

% message
fun_messages('pyramid levels','process');

% pyramid computation
for iterCell = minCell:maxCell
    
    % current resized image
    rsImg = mex_II2Img(II,iterCell);
    rsImg = fun_normalize(rsImg);
    
    % image size
    sy = size(rsImg,1);  % y
    sx = size(rsImg,2);  % x
    
    % small image size
    if (sy<=minImgSize || sx<=minImgSize), break; end
    
    % show resized images
    figure,imshow(rsImg);
    xlabel(sprintf('img. size -> [%dx%d]',sy,sx),'fontsize',fs);
    
    % message
    fun_messages(sprintf('cell -> %d : img. size -> [%dx%d]',iterCell,...
        sy,sx),'information');
    
end

% message
fun_messages('End','title');
end

%% messages
% This function prints a specific message on the command window
function fun_messages(text,message)
if (nargin~=2), error('incorrect input parameters'); end

% types of messages
switch (message)
    case 'presentation'
        fprintf('****************************************************\n');
        fprintf(' %s\n',text);
        fprintf('****************************************************\n');
        fprintf(' Michael Villamizar\n mvillami@iri.upc.edu\n');
        fprintf(' http://www.iri.upc.edu/people/mvillami/\n');
        fprintf(' Institut de Robòtica i Informàtica Industrial CSIC-UPC\n');
        fprintf(' c. Llorens i Artigas 4-6\n 08028 - Barcelona - Spain\n 2014\n');
        fprintf('****************************************************\n\n');
    case 'title'
        fprintf('****************************************************\n');
        fprintf('%s\n',text);
        fprintf('****************************************************\n');
    case 'process'
        fprintf('-> %s\n',text);
    case 'information'
        fprintf('->     %s\n',text);
    case 'warning'
        fprintf('-> %s !!!\n',text);
    case 'error'
        fprintf(':$ ERROR : %s\n',text);
        error('program error');
end
end

%% normalize image
% This function normalizes the input image in the range [0,1].
function img = fun_normalize(img)
% normalize
img = double(img);
img = img - min(img(:));
img = img./max(img(:));
end

